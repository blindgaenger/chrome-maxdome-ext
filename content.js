var getAsset = function(assetId, callback) {
  var assetUrl = 'https://www.maxdome.de/' + assetId;
  requestDoc(assetUrl, function(err, doc) {
    if (err) return callback(err);

    var rentButton = doc.querySelector('[data-asset="' + assetId + '"][data-order-type="rent"] .first-row')

    var asset = {
      id: assetId,
      url: doc.querySelector('link[rel="canonical"]').getAttribute('href') || assetUrl,
      title: doc.querySelector('h1').innerText.trim(),
      rating: parseInt(doc.querySelector('[data-asset-id="' + assetId + '"][data-asset-rating]').getAttribute('data-asset-rating')),
      included: rentButton && rentButton.innerText.trim() == 'In Ihrem Paket'
    }

    callback(null, asset)
  })
}

var findOrCreateInfoSpan = function(assetId) {
  var span = document.querySelector('.content[data-asset-id="' + assetId + '"] .info')
  if (span) return span;

  span = document.createElement('span');
  span.className = 'info';
  span.style.cssText = 'position: absolute; color: white; font-size: 15px; left: 6px; bottom: 15px; width: 137px; height: 20px; text-align: center;'

  var assetLink = document.querySelector('.content[data-asset-id="' + assetId + '"] a')
  assetLink.appendChild(span);

  return span;
}

var ratingText = function(rating) {
  var str = ''
  for (var i = 0; i < rating; i++) str += '★';
  for (var i = 0; i < 5-rating; i++) str += '☆';
  return str;
}

var assetNodes = document.querySelectorAll('.content[data-asset-id]')
for (var i = 0; i < assetNodes.length; i++) {
  var assetId = assetNodes[i].getAttribute('data-asset-id')

  findOrCreateInfoSpan(assetId).style.background = 'yellow'

  getAsset(assetId, function(err, asset) {
    if (err) return log('Error: ' + err.message);

    var span = findOrCreateInfoSpan(asset.id);
    span.style.background = asset.included ? 'green' : 'red';
    span.innerText = ratingText(asset.rating)
  })
};

