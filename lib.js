// Note that any URL fetched here must be matched by a permission in the manifest.json file!
var requestData = function(url, callback) {
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function(data) {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        var data = xhr.responseText;
        callback(null, data);
      } else {
        var err = {status: xhr.status, message: xhr.statusText}
        callback(err);
      }
    }
  }
  xhr.open('GET', url, true);
  xhr.send();
}

var requestDoc = function(url, callback) {
  requestData(url, function(err, data) {
    if (err) return callback(err);

    var div = document.createElement('div');
    div.innerHTML = data;

    var doc = document.createDocumentFragment();
    doc.appendChild(div);

    callback(null, doc)
  })
}
